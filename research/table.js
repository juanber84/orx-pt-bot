const contrib = require('blessed-contrib')

var table = contrib.table(
    {
        keys: true
        , fg: 'white'
        , selectedFg: 'white'
        , selectedBg: 'blue'
        , interactive: true
        , label: 'Active Processes'
        , width: '30%'
        , height: '30%'
        , border: { type: "line", fg: "cyan" }
        , columnSpacing: 10 //in chars
        , columnWidth: [16, 12, 12] /*in chars*/
    })


table.focus()

table.setData(
    {
        headers: ['col1', 'col2', 'col3']
        , data:
            [[1, 2, 3]
                , [4, 5, 6]]
    })