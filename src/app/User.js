'use strict'

const _ = require('lodash')
const uuid = require('node-uuid')
const faker = require('faker')

const Request = require('./Request')
const notice = require('./notice')

let minInability = 500
let maxInability = 2000

class User {

    constructor() {
        this.uuid = uuid.v1()
        this.debug = require('debug')('orx-bot:' + this.uuid)
        this.request = new Request()
        this.promoConfiguration = {}
        this.idPromo = null
        this.authentication = null
        this.bearer = null
        this.steps = null
        this.actions = actions()
    }

    async greet() {
        this.debug('greet', 'hi')
        this.next()
    }

    async by() {
        this.debug('by', 'by')
    }

    async getPromo() {
        let availablePromotions = notice
        let index = _.random(0, availablePromotions.length - 1)
        this.idPromo = availablePromotions[index]
        this.debug('getPromo', this.idPromo)
        this.next()
    }

    async getPromoConfiguration() {
        let promoConfiguration = await this.request.getPromoConfiguration(this.idPromo)
        if (promoConfiguration) {
            this.promoConfiguration = promoConfiguration
            this.debug('promoConfiguration', JSON.stringify(Object.keys(this.promoConfiguration)))
            this.next()
        } else
            this.by()
    }

    async getAuthentication() {
        let authentication = _.get(this, 'promoConfiguration.authentication[0]')
        if (authentication && authentication.type) {
            this.authentication = authentication
            this.debug('getAuthentication', JSON.stringify(this.authentication))
            this.next()
        } else
            this.by()
    }

    async authenticate() {
        if (this.authentication.type && this.authentication.type === 'customForm') {
            // TODO: increments capability
            var randomEmail = this.uuid + '_' + faker.internet.email()
            let crmId = randomEmail
            let customFields = {
                email: randomEmail
            }
            let response = await this.request.authenticate(_.get(this, 'promoConfiguration.project'), crmId, customFields)
            this.bearer = _.get(response, 'value')
            this.debug('authenticate', this.bearer)
            this.next()
        } else
            this.by()

        // TODO: implements new types
    }

    async getSteps() {
        let steps = _.get(this, 'promoConfiguration.steps')
        if (steps && steps.length) {
            this.steps = steps
            this.debug('getSteps', JSON.stringify(this.steps))
            this.next()
        } else
            this.by()
    }

    async participate() {
        let participation = {
            acceptLegal: true
        }
        let result = await this.request.participate(this.bearer, this.idPromo, participation)
        this.debug('participate', JSON.stringify(result))
        if (_.random(0,1) === 1) {
            this.participate()
        } else {
            this.next()
        }
    }

    init() {
        this.next()
    }

    async next() {
        let action = this.actions.next()
        if (!action.done) {
            await inability() 
            await this[action.value]()
        } else
            this.by()
    }
}

function* actions() {
    yield 'greet'
    yield 'getPromo'
    yield 'getPromoConfiguration'
    yield 'getAuthentication'
    yield 'authenticate'
    yield 'getSteps'
    yield 'participate'
}

async function inability() {
    let inabilityUser = _.random(minInability, maxInability) 
    return new Promise(function (resolve) {
        setTimeout(resolve, inabilityUser)
    })
}

module.exports = User