'use strict'

let minConcurrence = 1
let maxConcurrence = 10
let intervalConcurrence = 3000

const User = require('./User')

const _ = require('lodash')

function god(total) {
    let population = _.random(minConcurrence, maxConcurrence)
    for (let index = 0; index < population ; index++) {
        let user = new User()
        user.init()
    }
}

module.exports = {
    start() {
        setInterval(god, intervalConcurrence)
    }
}