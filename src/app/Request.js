'use strict'

const parameters = require('./../parameters')
const axios = require('axios')
const _ = require('lodash')

class Request {

    constructor() {
        this.coreInstance = axios.create({
            baseURL: parameters.coreApiUrl,
            timeout: 5000,
            headers: { 
                'Content-Type': 'application/json',
                'X-app-sdk': 'WEB_1.0.0'
            }
        })
        this.ptInstance = axios.create({
            baseURL: parameters.ptApiUrl,
            timeout: 5000,
            headers: { 
                'Content-Type': 'application/json'
            }
        })
    }

    async authenticate(credentials, crmId, customFields) {

        // Get client token
        let authWeb = await this.coreInstance.post(`/v1/security/token`, {
            "grantType": "auth_web",
            credentials
        })
            .then(response => {
                return _.get(response, 'data.data')
            })

        let clientToken = _.get(authWeb, 'value')

        // Get auth token
        let bearer = await this.coreInstance.post(`/v1/security/token`, {
            grantType: "auth_user",
            credentials: {
                clientToken,
                crmProvider: {},
                crmId
            }
        })
            .then(response => {
                return _.get(response, 'data.data')
            })


        // Configure
        this.coreInstance.defaults.headers.common['Authorization'] = `Bearer ${bearer.value}`;
        await this.coreInstance.post(`/v1/configuration?sections=crm`, { 
            "crm": { customFields } 
        })
            .then(response => {
                return _.get(response, 'data.data')
            })

        return Promise.resolve(bearer)
    }

    async getPromoConfiguration(idPromo) {
        return this.ptInstance.get(`/configuration/${idPromo}`)
            .then(response => {
                return _.get(response, 'data.data')
            })
    }

    async participate(authentication, idPromo, participation){
        this.ptInstance.defaults.headers.common['Authorization'] = `Bearer ${authentication}`;
        return this.ptInstance.post(`/participate/${idPromo}`, participation)
            .then(response => {
                let data = _.get(response, 'data')
                if (data)
                    return data
                return { status: false, error: 'unknown' }
            })
            .catch(err => {
                let data = _.get(err, 'response.data')
                if (!data)
                    data = { status: false, error: 'unknown' }
                return Promise.resolve(data)
            })
    }
}

module.exports = Request
