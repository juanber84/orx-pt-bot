'use strict'

describe('User', function () {

    beforeAll(() => {
        let User = require('./../../src/app/User')
        this.user = new User()
        return Promise.resolve()
    })

    beforeEach(() => {
        // let generateToken = Date.now() + ''
        // this.generateToken = generateToken
        return Promise.resolve()
    })

    it('greet()', async () => {
        expect(this.user.greet()).toBe('hi')
    })

    it('getPromoConfiguration()', async () => {
        expect(this.user.getPromoConfiguration('123456')).resolves.toBe('hi')
    })

})